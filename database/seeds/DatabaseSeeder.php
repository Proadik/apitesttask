<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        DB::table('categories')->truncate();

        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);

        $products = Product::all();
        $products->each(function($product) {
            factory(\App\ProductCategory::class, 5)->create(['product_id' => $product->id]);
        });
    }
}
