<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\ProductCategory::class, function (Faker $faker) {
    return [
        'category_id' => function() {
            return \App\Category::all()->random()->id;
        },
        'product_id' => function() {
            return factory('App\Product')->create()->id;
        },
    ];
});
