<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
        'description' => $faker->paragraph,
        'price' => $faker->numberBetween(1500, 150000),
        'published' => $faker->randomElement([Product::PUBLISHED, Product::NOT_PUBLISHED]),
        'deleted' => $faker->randomElement([Product::DELETED, Product::NOT_DELETED])
    ];
});
