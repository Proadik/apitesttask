<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    protected $table = 'product_categories';

    public $timestamps = false;

    protected $fillable = ['category_id', 'product_id'];

}
