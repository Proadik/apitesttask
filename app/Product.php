<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    const DELETED = 1;
    const NOT_DELETED = 0;

    protected $table = 'products';

    protected $fillable = ['name', 'description', 'price', 'deleted', 'published'];

    public function scopeNotDeleted($query) {
        return $query->where('deleted', self::NOT_DELETED);
    }

    public function scopeDeleted($query) {
        return $query->where('deleted', self::DELETED);
    }

    public function category() {
        return $this->belongsToMany('App\Category', 'product_categories');
    }

}
