<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {

        $products = Product::query()->with('category');

        if($request->has('filters')) {

            foreach($request->get('filters') as $filter => $value) {

                switch ($filter) {
                    case 'name' :
                        $products->where('name', 'like', '%'.$value.'%');
                        break;

                    case 'category_id' :
                        $products->whereHas('category', function($q) use ($value) {
                            $q->where('category_id', $value);
                        });
                        break;

                    case 'category_name' :
                        $products->whereHas('category', function($q) use ($value) {
                            $q->where('name', $value);
                        });
                        break;

                    case 'price' :
                        $price_array = explode('-', $value);
                        $from = intval($price_array[0]);
                        $to = intval($price_array[1]);
                        $products->whereBetween('price', [$from ,$to]);
                        break;

                    case 'published' :
                        $products->where('published', $value);
                        break;

                    case 'deleted' :
                        $products->where('deleted', $value);
                        break;

                }

            }

        }

        $products = $products->get();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if(
            empty($data['name']) ||
            empty($data['description']) ||
            empty($data['price']) ||
            empty($data['categories'])
        )
        {
            return response()->json(['status' => 'error', 'message' => 'Заполнены не все поля'], 200);
        }

        $product = Product::create($request->except('categories'));

        $categories = explode(',', $data['categories']);

        if(count($categories) < 2 || count($categories) > 10) {
            return response()->json(['status' => 'error', 'message' => 'Вы можете загрузить от 2-х до 10 категорий'], 200);
        }

        foreach($categories as $category) {
            $formatted_category = trim($category);
            $formatted_category = str_replace(' ', '', $formatted_category);

            // если нет такой категорий, то удаляем созданный продукт и выкидываем ошибку
            if(empty(Category::find($formatted_category))) {
                $product->delete();
                return response()->json(['status' => 'error', 'message' => 'Категория с id = '.$formatted_category.' не найдена.' ], 200);
            }

            $product->category()->attach($formatted_category);
        }

        $full_product_info = Product::with('category')->find($product->id);
        return response()->json(['status' => 'ok', 'product' => $full_product_info], 200);
    }

    public function show($id)
    {
        $full_product_info = Product::with('category')->find($id);

        if(empty($full_product_info))
            return response()->json(['status' => 'error', 'message' => 'Товар не найден']);

        return response()->json(['status' => 'ok', 'product' => $full_product_info], 200);
    }

    public function update(Request $request, $id)
    {
        $full_product_info = Product::with('category')->find($id);
        $data = $request->all();

        if(empty($full_product_info))
            return response()->json(['status' => 'error', 'message' => 'Товар не найден']);

        $full_product_info->category()->detach();

        $categories = explode(',', $data['categories']);

        if(count($categories) < 2 || count($categories) > 10) {
            return response()->json(['status' => 'error', 'message' => 'Вы можете загрузить от 2-х до 10 категорий'], 200);
        }

        foreach($categories as $category) {
            $formatted_category = trim($category);
            $formatted_category = str_replace(' ', '', $formatted_category);

            // если нет такой категорий, то удаляем созданный продукт и выкидываем ошибку
            if(empty(Category::find($formatted_category))) {
                $full_product_info->delete();
                return response()->json(['status' => 'error', 'message' => 'Категория с id = '.$formatted_category.' не найдена.' ], 200);
            }

            $full_product_info->category()->attach($formatted_category);
        }

        $full_product_info->update($request->except('categories'));
        return response()->json(['status' => 'ok', 'message' => 'Товар успешно обновлен', 'product' => $full_product_info], 200);
    }

    public function destroy($id)
    {
        $full_product_info = Product::with('category')->find($id);

        if(empty($full_product_info))
            return response()->json(['status' => 'error', 'message' => 'Товар не найден']);

        $full_product_info->update(['deleted' => Product::DELETED]);
        return response()->json(['status' => 'ok', 'message' => 'Товар успешно удален'], 200);
    }
}
