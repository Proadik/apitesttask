<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index()
    {
        return response()->json(Category::all());
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if(empty($data['name'])) {
            return response()->json(['status' => 'error', 'message' => 'Заполнены не все поля'], 200);
        }

        $category = Category::create($data);
        return response()->json(['status' => 'ok', 'category' => $category], 200);
    }

    public function show($id)
    {
        $category = Category::find($id);

        if(empty($category))
            return response()->json(['status' => 'error', 'message' => 'Категория не найдена'], 200);

        return response()->json(['status' => 'ok', 'category' => $category], 200);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $data = $request->all();

        if(empty($category))
            return response()->json(['status' => 'error', 'message' => 'Категория не найдена'], 200);

        $category->update($data);
        return response()->json(['status' => 'ok', 'message' => 'Категория успешно обновлена'], 200);
    }

    public function destroy($id)
    {
        $category = Category::find($id);

        if(empty($category))
            return response()->json(['status' => 'error', 'message' => 'Категория не найдена'], 200);

        if($category->products->count() > 0) {
            return response()->json(['status' => 'error', 'message' => 'Категория привязана к продуктам. Нельзя удалить']);
        }

        $category->delete();
        return response()->json(['status' => 'ok', 'message' => 'Категория успешно удалена']);
    }
}
