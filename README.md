<p>Steps to install: </p>
<ol>
    <li>clone</li>
    <li>composer install</li>
    <li>php artisan key:generate</li>
    <li>.env database settings</li>
    <li>php artisan db:seed</li>
    <li>postman to test</li>
    <li>profit</li>
</ol>
