<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::resource('category', 'CategoryController');
Route::resource('product', 'ProductController');
